class AddEndedAtToRoad < ActiveRecord::Migration[5.0]
  def change
    add_column :roads, :endedAt, :datetime
  end
end

class CreateSpots < ActiveRecord::Migration[5.0]
  def change
    create_table :spots do |t|
      t.float :lat
      t.float :lng
      t.float :distance
      t.text :text
      t.string :emotion
      t.string :address

      t.timestamps
    end
  end
end

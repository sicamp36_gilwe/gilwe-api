class AddAlertToSpot < ActiveRecord::Migration[5.0]
  def change
    add_column :spots, :alert, :string
  end
end

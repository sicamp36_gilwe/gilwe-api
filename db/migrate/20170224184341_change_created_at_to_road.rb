class ChangeCreatedAtToRoad < ActiveRecord::Migration[5.0]
  def change
    rename_column :roads, :createdAt, :startedAt
  end
end

class CreateRoads < ActiveRecord::Migration[5.0]
  def change
    create_table :roads do |t|
      t.string :title
      t.string :address
      t.float :distance
      t.integer :duration
      t.datetime :createdAt
      t.datetime :updatedAt
      t.integer :bookmarkCount

      t.timestamps
    end
  end
end

class AddProfileUrlToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :profileUrl, :string
  end
end

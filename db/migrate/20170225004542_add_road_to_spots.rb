class AddRoadToSpots < ActiveRecord::Migration[5.0]
  def change
    add_reference :spots, :road, foreign_key: true
  end
end

class RemoveEmotionToSpot < ActiveRecord::Migration[5.0]
  def change
    remove_column :spots, :emotion
  end
end

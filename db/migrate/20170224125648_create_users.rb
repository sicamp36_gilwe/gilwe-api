class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :accessToken
      t.string :keyType
      t.string :userId
      t.string :name

      t.timestamps
    end

    add_index :users, [:accessToken, :keyType]
  end
end

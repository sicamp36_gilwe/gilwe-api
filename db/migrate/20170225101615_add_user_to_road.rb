class AddUserToRoad < ActiveRecord::Migration[5.0]
  def change
    add_reference :roads, :user, foreign_key: true
  end
end

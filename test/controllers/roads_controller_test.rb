require 'test_helper'

class RoadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @road = roads(:one)
  end

  test "should get index" do
    get roads_url, as: :json
    assert_response :success
  end

  test "should show road" do
    get road_url(@road), as: :json
    assert_response :success
  end

  test "should update road" do
    patch road_url(@road), params: { road: { address: @road.address, bookmarkCount: @road.bookmarkCount, likeCount: @road.likeCount, startedAt: @road.startedAt, distance: @road.distance, duration: @road.duration, title: @road.title, updatedAt: @road.updatedAt, endedAt: @road.endedAt  } }, as: :json
    assert_response 200
  end

  test "should destroy road" do
    assert_difference('Road.count', -1) do
      delete road_url(@road), as: :JSON
    end

    assert_response 204
  end

  test "should road start" do
    post '/roads/start', headers: { "accessToken": '1', "keyType": "facebook"}
  end
end

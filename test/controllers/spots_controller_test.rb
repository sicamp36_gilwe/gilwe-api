require 'test_helper'

class SpotsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @spot = spots(:one)
  end

  test "should get index" do
    get spots_url, as: :json, params: { road_id: @spot.road_id }
    assert_response :success
  end

  test "should create spot" do
    assert_difference('Spot.count') do
      post spots_url, params: { lat: spots(:two).lat+1, lng: spots(:two).lng+1, road_id: @spot.road_id }, as: :json
    end
    assert_not_equal response.parsed_body['distance'], 0
    assert_response 201
  end

  test "should show spot" do
    get spots_url(@spot), as: :json, params: { road_id: @spot.road_id}
    assert_response :success
  end

  test "should update spot" do
    patch spot_url(@spot), params: { distance: @spot.distance, lat: @spot.lat, lng: @spot.lng, text: @spot.text, road_id: @spot.road_id }, as: :json
    assert_response 200
  end

end

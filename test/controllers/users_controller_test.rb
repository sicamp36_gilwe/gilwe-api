require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { accessToken: 'new-user', keyType: 'facebook', name: 'test', userId: 'testes', profileUrl: 'profile' } , as: :json
    end
    assert_response 201
  end

  test "should login success" do
    post '/login', params: { accessToken: @user.accessToken, keyType: @user.keyType}, as: :json
    assert_response 200
  end

  test "should login fail" do
    post '/login', params: { accessToken: 'not-found-user', keyType: 'facebook' }, as: :json
    assert_response 200
    assert_equal response.parsed_body['code'], 404
  end
end

class UsersController < ApplicationController

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: {code: 200, user: @user}, status: :created, location: @user
    else
      render json: {code: 400, user: @user}, status: :unprocessable_entity
    end
  end

  def login
    @user = User.find_by(accessToken: params[:accessToken], keyType: params[:keyType])
    if @user
      render json: {code: 200, message: 'Login OK '}, status: :ok
    else
      render json: {code: 404, message: 'not found user'}, status: :ok
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:accessToken, :keyType, :userId, :name, :profileUrl)
    end
end

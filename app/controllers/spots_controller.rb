class SpotsController < ApplicationController
  before_action :set_spot, only: [:show, :update, :destroy]
  before_action :set_road
  after_action :update_road_distance, only: [:create]
  after_action :set_address, only: [:create]

  DAUM_API = "https://apis.daum.net/local/geo/coord2addr?apikey=d8f946052530282be6ed0a376354ee9a&"

  # GET /spots
  def index
    @spots = Spot.all

    render json: @spots
  end

  # GET /spots/1
  def show
    render json: @spot
  end

  # POST /spots
  def create

    # 등록객체를 복사함
    new_params = spot_params.dup
    new_params['distance'] = getDistance

    @spot = Spot.new(new_params)

    if @spot.save
      render json: @spot, status: :created, location: @spot
    else
      render json: @spot.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /spots/1
  def update
    if @spot.update(spot_params)
      render json: @spot
    else
      render json: @spot.errors, status: :unprocessable_entity
    end
  end


  def bulk_spots

    param = params.permit(:road_id, :spots=> [:lat, :lng, :image, :text])
    #

    param['spots'].each do |spot|
      puts spot

      new_spot = spot.dup
      new_spot['road_id'] = params[:road_id]
      new_spot['distance'] = getDistance
      temp_spot = Spot.new(new_spot)
      temp_spot.save
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_spot
      @spot = Spot.find(params[:id])
    end

    def set_road
      @road = Road.find(params[:road_id])
    end

    # Only allow a trusted parameter "white list" through.
    def spot_params
      params.permit(:id, :lat, :lng, :distance, :text, :alert, :road_id, :image, :image_cache)
    end

    def getDistance
      @before_spot = Spot.where(:road_id => params[:road_id]).order('id asc').limit(1)
      if !@before_spot.empty?
        return Geokit::LatLng.new(@before_spot[0][:lat], @before_spot[0][:lng])
                       .distance_to(Geokit::LatLng.new(params[:lat], params[:lng])).round(3)
      else
        return 0
      end
    end

    def update_road_distance
      now = Time.now
      @road.update(distance: @spot['distance'], duration: (((now - @road['startedAt']) / 60) %60).to_i)
    end

    def set_address
      if @road['address'].nil?
        api_url = "#{DAUM_API}&longitude=#{params[:lng]}&latitude=#{params[:lat]}&inputCoordSystem=WGS84&output=json"
        response = HTTParty.get(api_url)
        print(response)
        print(response.body['fullName'])
        if !response.nil? && response['fullName']
          @road.update(address: response['fullName'])
        end
      end
    end
end

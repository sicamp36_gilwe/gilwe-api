class RoadsController < ApplicationController
  before_action :set_road, only: [:show, :update, :destroy, :endRoad]
  before_action :set_user, only: [:startRoad, :roadByUser]

  # GET /roads
  def index
    @roads = Road.all
    recommend = Array.new
    reads = Array.new
    see = Array.new
    new = Array.new
    my = Array.new

    @roads.sample(4).each do |road|
      recommend.push(RoadSerializer.new(road).as_json)
    end

    @roads.sample(3).each do |road|
      reads.push(RoadSerializer.new(road).as_json)
    end

    @roads.sample(3).each do |road|
      see.push(RoadSerializer.new(road).as_json)
    end

    @roads.sample(3).each do |road|
      new.push(RoadSerializer.new(road).as_json)
    end

    @roads.sample(3).each do |road|
      my.push(RoadSerializer.new(road).as_json)
    end

    render json: {
      recommends: recommend,
      reads: reads,
      sees: see,
      news: new,
      mys: my
    }
  end

  # GET /users/1/roads
  def roadByUser
    if @user
      roads_for_user_profile = Array.new
      @user.roads.each do |road|
        roads_for_user_profile.push(RoadSerializer.new(road).as_json)
      end
      render json: roads_for_user_profile, status: :ok
    else
      render json: [], status: :ok
    end


  end

  # GET /roads/1
  def show
    render json: RoadSerializer.new(@road).as_json
  end

  # POST /roads/start
  def startRoad

    @road = @user.roads.new({startedAt: Time.now, title: params[:title]})

    if @road.save
      render json: RoadSerializer.new(@road).as_json, status: :ok
    else
      render status: :bad_request
    end

  end

  # POST /roads/end
  def endRoad
    now = Time.now
    @road.update(title: params[:title], endedAt: now, duration: (((now - @road['startedAt']) / 60) %60).to_i)
    render json: RoadSerializer.new(@road).as_json, status: :ok
  end

  # PATCH/PUT /roads/1
  def update
    if @road.update(road_params)
      render json: @road
    else
      render json: @road.errors, status: :unprocessable_entity
    end
  end

  # DELETE /roads/1
  def destroy
    @road.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_road
      @road = Road.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def road_params
      params.require(:road).permit(:title, :address, :distance, :duration, :startedAt, :updatedAt, :bookmarkCount, :likeCount, :endedAt)
    end

    def set_user
      @user = User.find_by(accessToken: request.headers[:accessToken], keyType: request.headers[:keyType])
    end
end

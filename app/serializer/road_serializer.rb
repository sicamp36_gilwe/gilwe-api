class RoadSerializer

  attr_reader :road

  def initialize(road)
    @road = road
  end

  def as_json
    getSpots
    {
      id: road.id,
      title: road.title,
      address: road.address,
      bookMarkCount: road.bookmarkCount,
      isBookMarked: road.bookmarkCount.to_i > 0 ? true : false,
      duration: road.duration,
      distance: road.distance,
      text: getTitle,
      image: getImage,
      user: {
          id: road.user.id,
          name: road.user.name,
          profileUrl: road.user.profileUrl
      },
      spots: groupSpots
    }
  end

  private
    def getSpots
      @spots_text = @road.spots.select { |hash| !hash[:text].nil? }
      @spots_image = @road.spots.select { |hash| !hash[:image].nil? }
    end


    def getTitle
      return @spots_text.empty? ? '' : @spots_text.first['text']
    end

    def getImage
      image = @spots_image.first
      return @spots_image.empty? ? '' : "http://gilwe.com:3000/uploads/spot/image/#{image['id']}/#{image['image']}"
    end

    def groupSpots
      groups = Hash.new
      if @road.spots.size > 0
        @road.spots.each do |spot|
          group_key = (spot[:distance] * 1000 / 100).to_i*100
          if !groups.key?(group_key)
            print('.')
            groups[group_key] = Array.new
          end
          groups[group_key].push(SpotSerializer.new(spot).as_json)

        end
        return Hash[groups.sort].values
      else
        return []
      end
    end
end
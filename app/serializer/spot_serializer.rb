class SpotSerializer

  attr_reader :spot

  def initialize(spot)
    @spot = spot
  end

  def as_json
    {
      id: spot.id,
      lat: spot.lat,
      lng: spot.lng,
      distance: spot.distance,
      image: getImage,
      text: spot.text,
      alert: spot.alert
    }
  end

  private
    def getImage
      @spot['image'].nil? ? '' : "http://gilwe.com:3000/uploads/spot/image/#{@spot['id']}/#{@spot['image']}"
    end

end
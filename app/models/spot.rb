class Spot < ApplicationRecord

  belongs_to :road
  mount_uploader :image, ImageUploader

end

Rails.application.routes.draw do
  resources :spots
  resources :roads
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post '/login', to: 'users#login'
  post '/roads/start', to: 'roads#startRoad'
  post '/roads/end', to: 'roads#endRoad'
  get '/users/:user_id/roads', to: 'roads#roadByUser'
  post '/spots/bulk/:road_id', to: 'spots#bulk_spots'
end

# config valid only for current version of Capistrano
lock "3.7.2"

set :application, 'gilwe'
set :repo_url, 'git@bitbucket.org:sicamp36_gilwe/gilwe-api.git'
set :deploy_to, '/var/www/gilwe'

ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :use_sudo, false
set :bundle_binstubs, nil
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :config_files, fetch(:linked_files)

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  task :restart do
    invoke 'unicorn:reload'
  end
end

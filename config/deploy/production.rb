set :port, 22
set :user, 'root'
set :deploy_via, :remote_cache
set :use_sudo, false

server 'gilwe.com',
       roles: [:web, :app, :db],
       port: fetch(:port),
       user: fetch(:user),
       primary: true

set :deploy_to, "/var/www/gilwe"

set :ssh_options, {
    forward_agent: true,
    auth_methods: %w(publickey),
    user: 'root',
}

set :rails_env, :production
set :conditionally_migrate, true